const video = document.querySelector('.player');
const canvas = document.querySelector('.photo');
const ctx = canvas.getContext('2d');
const strip = document.querySelector('.strip');
const snap = document.querySelector('.snap');

function getVideo() {
    navigator.mediaDevices.getUserMedia({video: true, audio: false})
        .then(localMediaStream => {
        console.log(localMediaStream);
            video.srcObject = localMediaStream;
            video.play();
    }).catch(err => {
        console.error(`OH NO!!!`, err);
    });
}

function paintToCanvas() {
    const width = video.videoWidth;
    const height = video.videoHeight;

    //set canvas dimension to incoming video stream dimension
    canvas.width = width;
    canvas.height = height;

    //pass webcam stream photo to canvas every 16ms
    return setInterval(() => {
        ctx.drawImage(video, 0, 0, width, height);

        //take pixels out of canvas
        let pixels = ctx.getImageData(0, 0, width, height);

        //mess with pixels
  /*      pixels = rgbSplit(pixels);
        ctx.globalAlpha = 0.2;*/

        pixels = greenScreen(pixels);

        //put them back into canvas
        ctx.putImageData(pixels, 0, 0)
    }, 16)
}

function takePhoto() {
    //play snap audio
    snap.currentTime = 0;
    snap.play();

    //export snapshot
    const data = canvas.toDataURL('image/jepg');
    const link = document.createElement('a');
    link.href = data;
    link.setAttribute('download', 'beauty');
    link.innerHTML = `<img src ="${data}" alt="Beauty" />`;
    strip.insertBefore(link, strip.firstChild);
}

function redEffect(pixels) { //shift pixel rgb values
    for (let i = 0; i < pixels.data.length; i+=4) {
        pixels[i + 0] = pixels.data[i + 0] + 200; //red
        pixels[i + 1] = pixels.data[i + 1] - 50; //green
        pixels[i + 2] = pixels.data[i + 2] * 0.5; //blue
    }
    return pixels;
}

function rgbSplit(pixels) {
  //change place of rgb values
  for (let i = 0; i < pixels.data.length; i += 4) {
    pixels[i - 150] = pixels.data[i + 0]; //red
    pixels[i + 100] = pixels.data[i + 1]; //green
    pixels[i - 150] = pixels.data[i + 2]; //blue
  }
  return pixels;
}

function greenScreen(pixels) {
    const levels = {};

    document.querySelectorAll('.rgb input').forEach((input) => {
        levels[input.name] = input.value;
    });

    for (let i = 0; i < pixels.data.length; i = i + 4) {
        let red = pixels.data[i + 0];
        let green = pixels.data[i + 1];
        let blue = pixels.data[i + 2];
        let alpha = pixels.data[i + 3];

        //check if rgb values are in range
        if (red >= levels.rmin
            && green >= levels.gmin
            && blue >= levels.bmin
            && red <= levels.rmax
            && green <= levels.gmax
            && blue <= levels.bmax) {
            // take it out
            pixels.data[i + 3] = 0;
        }
    }

    return pixels;
}


getVideo();

//listen to webcam video stream
video.addEventListener('canplay', paintToCanvas);
